<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  public function index()
  {
    return Product::get();
  }

  public function store(Request $request)
  {
    $request->validate([
      'description' => ['required']
    ]);

    $product = Product::create($request->all());

    return $product;
  }

  public function show(Product $product)
  {
    return $product;
  }

  public function update(Product $product)
  {
    $product->fill(request()->all());
    $product->save();

    return $product;
  }

  public function destroy(Product $product)
  {
    $product->delete();
    return response(null, 204);
  }
}
